<?php

namespace Leonix\Shared\Application;

use ReflectionClass;
use ReflectionProperty;

trait InitializableFromArrayTrait
{
    public static function fromArray(array $data)
    {
        static $propertiesCache = null;
        $className = static::class;
        if ($propertiesCache === null || !isset($propertiesCache[$className])) {
            $reflection = new ReflectionClass($className);
            $public = $reflection->getProperties(ReflectionProperty::IS_PUBLIC);
            $static = $reflection->getProperties(ReflectionProperty::IS_STATIC);
            $propertiesCache[$className] = array_diff($public, $static);
        }

        $object = new $className();
        foreach ($propertiesCache[$className] as $property) {
            $object->{$property->name} = $data[$property->name] ?? null;
        }

        return $object;
    }
}
