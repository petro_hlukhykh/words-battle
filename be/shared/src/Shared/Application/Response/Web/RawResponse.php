<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Web;

use Leonix\Shared\Application\Enum\HttpStatusCode;
use Leonix\Shared\Application\Enum\ResponseType;

final class RawResponse implements ResponseInterface
{
    /**
     * @var string
     */
    private $body;
    /**
     * @var string
     */
    private $type;
    /**
     * @var int
     */
    private $status;

    private function __construct(string $body, string $type, int $status = HttpStatusCode::HTTP_OK)
    {
        $this->body = $body;
        $this->type = $type;
        $this->status = $status;
    }

    public static function makeXml(string $body): self
    {
        return new self($body, ResponseType::XML);
    }

    public static function makeHtml(string $body): self
    {
        return new self($body, ResponseType::HTML);
    }

    public function body(): string
    {
        return $this->body;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function status(): int
    {
        return $this->status;
    }
}
