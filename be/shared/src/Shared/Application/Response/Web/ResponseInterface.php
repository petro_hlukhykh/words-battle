<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Web;

interface ResponseInterface
{
    /**
     * @return string
     */
    public function body(): string;

    /**
     * @return int
     */
    public function status(): int;

    /**
     * @return string
     */
    public function type(): string;
}
