<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Api;

use JsonSerializable;
use Leonix\Shared\Application\Enum\HttpStatusCode;
use Leonix\Shared\Application\Enum\ResponseStatus;

final class SuccessApiResponse implements ResponseInterface
{
    // Backward compatibility hint JsonApi::resultOk()
    public const BC_HINT_RESULT_OK = 15;
    // Backward compatibility hint JsonApi::okData()
    public const BC_HINT_OK_DATA = 20;
    /**
     * @var array|iterable|JsonSerializable
     */
    private $data;
    /**
     * @var int
     */
    private $status;

    private $bcHint = self::BC_HINT_NO_HINT;

    private function __construct($data = [], int $status = HttpStatusCode::HTTP_OK)
    {
        assert(is_array($data) || is_iterable($data) || $data instanceof JsonSerializable);
        $this->data = $data;
        $this->status = $status;
    }

    /**
     * @param array|iterable|JsonSerializable $data
     * @param int $status
     * @return static
     */
    public static function makeFrom($data = [], int $status = HttpStatusCode::HTTP_OK): self
    {
        return new self($data, $status);
    }

    public static function noData(int $status = HttpStatusCode::HTTP_OK): self
    {
        return new self([], $status);
    }

    public function body(): array
    {
        return [
            'status' => ResponseStatus::SUCCESS,
            'data' => $this->data,
        ];
    }

    public function status(): int
    {
        return $this->status;
    }

    /**
     * @inheritDoc
     */
    public function withBcHint(int $hint = self::BC_HINT_RESULT_OK): ResponseInterface
    {
        $clone = clone $this;
        $clone->bcHint = $hint;

        return $clone;
    }

    public function bcHint(): int
    {
        return $this->bcHint;
    }
}
