<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Api;

interface ResponseInterface
{
    public const BC_HINT_NO_HINT = 0;

    /**
     * @return array
     */
    public function body(): array;

    /**
     * @return int
     */
    public function status(): int;

    /**
     * Should be used to hint backward compatibility flags
     * @param int $hint
     * @return ResponseInterface
     */
    public function withBcHint(int $hint): ResponseInterface;

    /**
     * Return backward compatibility hint
     * @return int
     */
    public function bcHint(): int;
}
