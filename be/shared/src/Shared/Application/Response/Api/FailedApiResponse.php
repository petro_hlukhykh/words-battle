<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Response\Api;

use Leonix\Shared\Application\Enum\HttpStatusCode;
use Leonix\Shared\Application\Enum\ResponseStatus;
use Leonix\Shared\Application\Service\ErrorFactory;
use Leonix\Shared\Domain\Enum\ErrorCode;
use Leonix\Shared\Domain\Model\Error;
use Throwable;

final class FailedApiResponse implements ResponseInterface
{
    // Backward compatibility hint JsonApi::resultFailure()
    public const BC_HINT_RESULT_FAILURE = 10;

    private $status;
    private $error;
    private $bcHint = self::BC_HINT_NO_HINT;

    public function __construct(Error $error, int $status = HttpStatusCode::HTTP_ERROR)
    {
        $this->status = $status;
        $this->error = $error;
    }

    /**
     * @param string $message
     * @param string $code
     * @param array $errors
     * @param int $httpStatus
     * @return static
     */
    public static function makeFrom(
        string $message,
        string $code = ErrorCode::GENERAL_ERROR,
        array $errors = [],
        int $httpStatus = HttpStatusCode::HTTP_ERROR
    ): self {
        return new self(new Error($message, $code, $errors), $httpStatus);
    }

    public static function makeFromThrowable(
        Throwable $throwable,
        array $data = [],
        int $httpStatus = HttpStatusCode::HTTP_ERROR
    ): self {
        return new self(new Error($throwable->getMessage(), (string)$throwable->getCode(), $data), $httpStatus);
    }

    public static function generalError(int $status = HttpStatusCode::HTTP_ERROR): self
    {
        return new self(ErrorFactory::generalError(), $status);
    }

    public static function validationError(array $errors, int $status = HttpStatusCode::HTTP_ERROR): self
    {
        return new self(ErrorFactory::validationError($errors), $status);
    }

    /**
     * @inheritDoc
     */
    public function withBcHint(int $hint = self::BC_HINT_RESULT_FAILURE): ResponseInterface
    {
        $clone = clone $this;
        $clone->bcHint = $hint;

        return $clone;
    }

    public function body(): array
    {
        return [
            'status' => ResponseStatus::FAILED,
            'error' => $this->error->jsonSerialize(),
        ];
    }

    public function status(): int
    {
        return $this->status;
    }

    public function error(): Error
    {
        return $this->error;
    }

    public function bcHint(): int
    {
        return $this->bcHint;
    }
}
