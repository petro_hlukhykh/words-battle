<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Enum;

final class ResponseStatus
{
    public const SUCCESS = 'success';

    public const FAILED = 'failed';
}
