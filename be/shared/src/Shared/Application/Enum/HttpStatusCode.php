<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Enum;

final class HttpStatusCode
{
    public const HTTP_OK = 200;

    public const HTTP_ERROR = 500;
}
