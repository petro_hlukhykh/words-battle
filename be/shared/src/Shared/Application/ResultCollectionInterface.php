<?php

declare(strict_types=1);

namespace Leonix\Shared\Application;

use Countable;
use IteratorAggregate;

interface ResultCollectionInterface extends Countable, IteratorAggregate
{
    public function getResult();

    public function getSingleResult();

    public function hydrateSingleResultAs(string $className);

    public function hydrateResultsAs(string $className): ResultCollectionInterface;
}
