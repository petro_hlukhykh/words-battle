<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

trait OrderingTrait
{
    /**
     * CSV fields to order by(max 2 fields):
     *  sort=id,productId will produce order by id, productId ASC
     *  sort=-id,productId will produce order by id, productId DESC
     *
     * @Assert\Regex(
     *     pattern="/^-?([a-z0-9.]+){1}(,-?[a-z0-9.]+)?$/i",
     *     message="Invalid sort format, expected: sort=-id,productId for DESC, sort=id,productId for ASC"
     * )
     * @var string|null
     */
    private $sort;

    /**
     * @var array
     */
    private $parsedFields;

    /**
     * @param OrderByColumn[] $default
     * @return array
     */
    public function sortFields(array $default = []): array
    {
        return $this->parse($default);
    }

    private function parse(array $default = []): array
    {
        if (!$this->sort) {
            return $default ?? [];
        }

        if ($this->parsedFields !== null) {
            return $this->parsedFields ?? $default;
        }

        $sortFields = array_filter(array_unique(array_map('trim', explode(',', rtrim($this->sort, ',')))));
        $this->parsedFields = [];
        foreach ($sortFields as $field) {
            if ($field[0] === '-') {
                $this->parsedFields[] = new OrderByColumn(substr($field, 1), true);
                continue;
            }

            $this->parsedFields[] = new OrderByColumn($field, false);
        }

        return $this->parsedFields;
    }

    public function withOrder(OrderByColumn $column)
    {
        $self = clone $this;
        $self->parsedFields[] = $column;

        return $self;
    }
}
