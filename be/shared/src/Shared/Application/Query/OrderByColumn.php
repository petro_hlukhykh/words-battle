<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

final class OrderByColumn
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var bool
     */
    private $descendant;

    public function __construct(string $name, bool $descendant = false)
    {
        $this->name = $name;
        $this->descendant = $descendant;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function isDescendant(): bool
    {
        return $this->descendant;
    }
}
