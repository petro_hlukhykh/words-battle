<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

use function Leonix\Shared\nullable_int;

trait PaginationTrait
{
    /**
     * @Assert\Type(type="array")
     * @Assert\Collection(
     *     fields={
     *         "limit"=@Assert\Required({@Assert\NotBlank, @Assert\Range(min="0", max="100")}),
     *         "skip"=@Assert\Optional(@Assert\Range(min="0"))
     *     },
     *     allowExtraFields=true
     * )
     * @var array
     */
    private $page;

    /**
     * @param string|null $relation
     * @param Page|null $default
     * @return Page
     */
    public function page(?string $relation = null, Page $default = null): Page
    {
        if (!$this->page) {
            return $default ?? new Page();
        }

        if ($relation) {
            if (isset($this->page[$relation])) {
                return new Page(
                    nullable_int($this->page[$relation]['limit'] ?? null),
                    nullable_int($this->page[$relation]['skip'] ?? null)
                );
            }

            return $default ?? new Page();
        }

        return new Page(nullable_int($this->page['limit'] ?? null), nullable_int($this->page['skip'] ?? null));
    }

    public function withPage(Page $page, ?string $relation = null)
    {
        $self = clone $this;
        $self->page ??= [];
        if (!$relation) {
            $self->page['limit'] = $page->limit();
            $self->page['skip'] = $page->skip();

            return $self;
        }

        $self->page[$relation] = ['limit' => $page->limit(), 'skip' => $page->skip()];

        return $self;
    }
}
