<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

use function Leonix\Shared\safe_explode;

/**
 * Parse request params like below:
 * filter[gameId:eq]=23&filter[provider][id:gt]=125&filter[clientId:nin]=23,456,44
 */
trait SimpleFilterTrait
{
    /**
     * @Assert\Type(type="array")
     * @Assert\Expression(
     *     "this.parseFilters(value) === true",
     *     message="Invalid filter"
     * )
     * @var array
     */
    private $filter;

    /**
     * @var array|null
     */
    private $parsedFilters;

    public function filters(string $resource = null): array
    {
        if (!$this->parsedFilters) {
            return [];
        }

        return $this->parsedFilters[$resource ?? 'default'] ?? [];
    }

    public function parseFilters($filters): bool
    {
        if ($filters === null) {
            return true;
        }

        if ($this->parsedFilters !== null) {
            return true;
        }

        if (!is_array($filters)) {
            return false;
        }

        $this->parsedFilters = $this->parseGroup($filters, 'default');

        return $this->parsedFilters !== null;
    }

    private function parseGroup(array $group, string $groupName): ?array
    {
        $parsed = [$groupName => []];
        foreach ($group as $key => $param) {
            [$column, $operator] = safe_explode(':', $key);
            if (!$operator) {
                if (!is_array($param)) {
                    return null;
                }

                $parsedGroup = $this->parseGroup($param, $column);
                if ($parsedGroup === null) {
                    return null;
                }

                $parsed = array_merge($parsed, $parsedGroup);
                continue;
            }

            if (!Filter::isSupported($operator)) {
                return null;
            }

            if (Filter::isArrayOperator($operator)) {
                $param = explode(',', $param);
            }

            $parsed[$groupName][] = Filter::createFilter($operator, $column, $param);
        }

        return $parsed;
    }

    public function withFilter(Filter $filter, ?string $resource = null)
    {
        $self = clone $this;
        $self->parsedFilters = $this->parsedFilters ?? [];
        $resource ??= 'default';
        $self->parsedFilters[$resource] ??= [];
        $self->parsedFilters[$resource][] = $filter;

        return $self;
    }
}
