<?php

declare(strict_types=1);

namespace Leonix\Shared\Application\Query;

use Symfony\Component\Validator\Constraints as Assert;

trait IncludesTrait
{
    /**
     * CSV relations to include: include=id,title
     * @Assert\Regex(
     *     pattern="/^(?:[a-z0-9.]+)*(?:[a-z0-9.]+,)*(?:[a-z0-9.]+)*$/i",
     *     message="Invalid include format, expected: include=id,productId"
     * )
     *
     * @var string
     */
    private $include;

    /**
     * @param array $default
     * @return array
     */
    public function includes(array $default = []): array
    {
        if (!$this->include) {
            return $default;
        }

        $includes = array_unique(array_map('trim', explode(',', rtrim($this->include, ','))));

        return $includes ?? $default;
    }
}
