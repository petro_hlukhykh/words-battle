<?php

declare(strict_types=1);

namespace Leonix\Shared\Application;

interface ConstructFromArrayInterface
{
    public static function fromArray(array $array);
}
