<?php

declare(strict_types=1);

namespace Leonix\Shared\Application;

use ArrayIterator;
use JsonSerializable;

use function count;
use function reset;

final class ResultCollection implements ResultCollectionInterface, JsonSerializable
{
    /**
     * @var array
     */
    private array $items;
    private ?string $itemHydrationClass;

    public function __construct(array $items, ?string $itemHydrationClass = null)
    {
        $this->items = $items;
        $this->itemHydrationClass = $itemHydrationClass;
    }

    public function getResult(): array
    {
        if (!$this->itemHydrationClass) {
            return $this->items;
        }
        return $this->hydrateResultsAs($this->itemHydrationClass)->getResult();
    }

    /**
     * @param string $className
     * @return mixed
     */
    public function hydrateSingleResultAs(string $className)
    {
        assert(class_exists($className) && is_callable($className . '::fromArray'));
        $item = $this->getSingleResult();

        return $className::fromArray($item);
    }

    public function getSingleResult()
    {
        return reset($this->items);
    }

    public function hydrateResultsAs(string $className): ResultCollectionInterface
    {
        assert(class_exists($className) && is_callable($className . '::fromArray'));
        $hydratedItems = [];
        foreach ($this->items as $item) {
            $hydratedItems[] = $className::fromArray($item);
        }

        return new self($hydratedItems);
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->getResult());
    }

    public function jsonSerialize(): array
    {
        return $this->items;
    }
}
