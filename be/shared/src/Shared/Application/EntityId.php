<?php

namespace Leonix\Shared\Application;

use RuntimeException;
use Symfony\Component\Validator\Constraints as Assert;

class EntityId
{
    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="1")
     * @var int
     */
    private $id;

    public function __construct($id = null)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        if (!$this->id) {
            throw new RuntimeException('Id does not initialized');
        }

        return $this->id;
    }
}
