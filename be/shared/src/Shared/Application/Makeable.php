<?php

declare(strict_types=1);

namespace Leonix\Shared\Application;

trait Makeable
{
    /**
     * @param ...$args
     * @return static
     */
    public static function make(...$args)
    {
        return new static(...$args);
    }
}
