<?php

namespace Leonix\Shared\Domain\Bus\Event;

use DateTimeImmutable;

interface DomainEvent
{
    /**
     * Retrieve unique domain name
     * @return string
     */
    public static function eventName(): string;

    /**
     * Retrieve unique domain ID
     * @return string
     */
    public function eventId(): string;

    /**
     * @return DateTimeImmutable
     */
    public function occurredOn(): DateTimeImmutable;
}
