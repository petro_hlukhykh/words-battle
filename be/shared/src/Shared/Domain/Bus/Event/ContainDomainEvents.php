<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

interface ContainDomainEvents
{
    /**
     * Fetch recorded events.
     *
     * @return DomainEvent[]
     */
    public function recordedEvents(): array;

    /**
     * Erase events that were recorded since the last call to eraseMessages().
     */
    public function eraseEvents(): void;
}
