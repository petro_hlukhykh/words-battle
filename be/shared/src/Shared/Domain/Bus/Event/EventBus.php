<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

interface EventBus
{
    /**
     * @param DomainEvent $event
     * @param array $context
     */
    public function notify(DomainEvent $event, array $context = []): void;
}
