<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

use RuntimeException;

final class EventSubscriberNotRegisteredError extends RuntimeException
{
    public function __construct(DomainEvent $event)
    {
        $eventClass = get_class($event);

        parent::__construct("The event <$eventClass> hasn't a event subscriber associated");
    }
}
