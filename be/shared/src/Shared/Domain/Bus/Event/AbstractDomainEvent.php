<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use RuntimeException;

abstract class AbstractDomainEvent implements DomainEvent
{
    private $eventId;
    private $aggregateId;
    private $data;
    private $occurredOn;

    protected function __construct(
        string $aggregateId,
        array $data = [],
        ?DateTimeImmutable $occurredOn = null,
        ?string $eventId = null
    ) {
        $this->aggregateId = $aggregateId;
        $this->data = $data;
        $this->occurredOn = $occurredOn ?? new DateTimeImmutable();
        $this->eventId = $eventId ?? Uuid::uuid4()->toString();
    }

    abstract public static function eventName(): string;

    public function __call($method, $args)
    {
        $attributeName = $method;
        if (strpos($method, 'is') === 0) {
            $attributeName = lcfirst(substr($method, 2));
        }

        if (strpos($method, 'has') === 0) {
            $attributeName = lcfirst(substr($method, 3));
        }

        if (strpos($method, 'get') === 0) {
            $attributeName = lcfirst(substr($method, 3));
        }

        if (isset($this->data[$attributeName])) {
            return $this->data[$attributeName];
        }

        throw new RuntimeException(sprintf('The method "%s" does not exist.', $method));
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function data(): array
    {
        return $this->data;
    }

    public function occurredOn(): DateTimeImmutable
    {
        return $this->occurredOn;
    }
}
