<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Event;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * In order to bind internal logic to our interface instead Symfony
 */
interface DomainEventHandler extends MessageHandlerInterface
{
}
