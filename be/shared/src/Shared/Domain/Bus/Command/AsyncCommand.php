<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Command;

interface AsyncCommand extends Command
{
}
