<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Command;

interface CommandBus
{
    /**
     * @param Command $command
     * @param array $context
     */
    public function dispatch(Command $command, array $context = []): void;
}
