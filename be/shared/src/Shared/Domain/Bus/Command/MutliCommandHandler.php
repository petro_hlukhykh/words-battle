<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Command;

use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

interface MutliCommandHandler extends MessageSubscriberInterface
{
}
