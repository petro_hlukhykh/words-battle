<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Query;

interface QueryBus
{
    /**
     * @param Query $query
     * @param array $context
     * @return Response|null
     */
    public function ask(Query $query, array $context = []): ?Response;
}
