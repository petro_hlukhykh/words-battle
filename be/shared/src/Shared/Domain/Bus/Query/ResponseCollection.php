<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Bus\Query;

use Leonix\Shared\Domain\Collection;

abstract class ResponseCollection extends Collection implements Response
{
}
