<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Webmozart\Assert\Assert;

abstract class Collection implements Countable, IteratorAggregate
{
    /**
     * @var array
     */
    private $items;

    public function __construct(array $items)
    {
        Assert::allIsInstanceOf($items, $this->type());
        $this->items = $items;
    }

    abstract protected function type(): string;

    public function getIterator()
    {
        return new ArrayIterator($this->items());
    }

    protected function items()
    {
        return $this->items;
    }

    public function count()
    {
        return count($this->items());
    }

    protected function each(callable $fn)
    {
        foreach ($this->items as $key => $value) {
            $fn($value, $key);
        }
    }
}
