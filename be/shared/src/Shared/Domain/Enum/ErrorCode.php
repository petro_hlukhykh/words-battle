<?php

declare(strict_types=1);

namespace Leonix\Shared\Domain\Enum;

final class ErrorCode
{
    public const GENERAL_ERROR = 'GENERAL_ERROR';
}
