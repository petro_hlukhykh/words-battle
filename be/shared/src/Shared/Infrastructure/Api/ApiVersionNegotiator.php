<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Api;

use Symfony\Component\HttpFoundation\Request;

interface ApiVersionNegotiator
{
    public function getVersion(Request $request): string;
}
