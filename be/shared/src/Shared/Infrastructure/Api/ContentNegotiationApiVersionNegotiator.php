<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Api;

use Symfony\Component\HttpFoundation\Request;

final class ContentNegotiationApiVersionNegotiator implements ApiVersionNegotiator
{
    private const VERSIONS = [
        'v1',
        'v1.0',
        'v1.1',
    ];

    private const MIME_TYPES = [
        'application/json',
        'application/vnd.api+json',
    ];

    private $acceptableMimeTypes;

    /**
     * @var string
     */
    private $defaultVersion;
    /**
     * @var string
     */
    private $defaultMimeType;
    /**
     * @var array|string[]
     */
    private $versions;

    public function __construct(
        string $defaultVersion = 'v1',
        string $defaultMimeType = 'application/json',
        array $versions = null
    ) {
        $this->defaultVersion = $defaultVersion;
        $this->defaultMimeType = $defaultMimeType;
        $this->versions = $versions ?? self::VERSIONS;
    }

    public function getVersion(Request $request): string
    {
        $accept = $request->headers->get('Accept') ?? $this->defaultMimeType;
        $this->initMimeTypes();

        return $this->normalizeVersion($this->acceptableMimeTypes[$accept] ?? $this->defaultVersion);
    }

    private function initMimeTypes(): void
    {
        if ($this->acceptableMimeTypes !== null) {
            return;
        }

        $this->acceptableMimeTypes = [];
        foreach ($this->versions as $version) {
            foreach (self::MIME_TYPES as $mimeType) {
                $this->acceptableMimeTypes[sprintf('%s;version=%s', $mimeType, $version)] = $version;
            }
        }
    }

    private function normalizeVersion(string $version): string
    {
        if (substr($version, -2) === '.0') {
            return substr($version, 0, -2);
        }

        return $version;
    }

    public function isAcceptableVersion(Request $request, string $maxVersion, ?string $minVersion = null): bool
    {
        $accept = $request->headers->get('Accept');
        if (!$accept) {
            return true;
        }

        $accept = $this->normalizeAcceptHeaderValue($accept);
        $this->initMimeTypes();
        if (!isset($this->acceptableMimeTypes[$accept])) {
            return false;
        }

        $requestVersion = $this->normalizeVersion($this->acceptableMimeTypes[$accept]);
        $acceptable = version_compare($maxVersion, $requestVersion, '>=');
        if (!$acceptable || !$minVersion) {
            return $acceptable;
        }

        return version_compare($minVersion, $requestVersion, '<=');
    }

    private function normalizeAcceptHeaderValue(string $value): string
    {
        return str_replace(' ', '', $value);
    }
}
