<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\ParamConverter\Mapper;

use Leonix\Shared\Infrastructure\Symfony\ParamConverter\Annotation\RequestParamMapper;
use Symfony\Component\HttpFoundation\Request;

interface RequestMapperInterface
{
    /**
     * Map all request properties using RequestParamMapper config.
     * Return an object.
     *
     * @param Request $request
     * @param RequestParamMapper $configuration
     * @return mixed
     */
    public function map(Request $request, RequestParamMapper $configuration);
}
