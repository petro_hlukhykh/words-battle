<?php

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Transport\AmqpTest;

use RuntimeException;
use Symfony\Component\Messenger\Transport\AmqpExt\Connection;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * !!!!ONLY FOR TEST PURPOSE
 */
class AmqpTestTransportFactory implements TransportFactoryInterface, TransportTestHelperInterface
{
    /**
     * @var AmqpTestTransport[]
     */
    private $transports = [];

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        $name = $options['transport_name'];
        unset($options['transport_name']);

        $transport = new AmqpTestTransport(Connection::fromDsn($dsn, $options), $serializer);
        $this->transports[$name] = $transport;

        return $transport;
    }

    public function supports(string $dsn, array $options): bool
    {
        return strpos($dsn, 'amqp-test://') === 0;
    }

    public function purge(): void
    {
        foreach ($this->transports as $transport) {
            $transport->purgeQueues();
        }
    }

    public function purgeTransport(string $transportName, ?string $queueName = null): void
    {
        /** @var AmqpTestTransport $transport */
        $transport = $this->getTransport($transportName);
        if (!$queueName) {
            $transport->purgeQueues();
        }
    }

    public function getTransport(string $transportName): TransportInterface
    {
        if (!$this->hasTransport($transportName)) {
            throw new RuntimeException("Transport {$transportName} is not registered.");
        }

        return $this->transports[$transportName];
    }

    public function hasTransport(string $transportName): bool
    {
        return isset($this->transports[$transportName]);
    }

    public function getMessageCount(string $transportName): int
    {
        /** @var AmqpTestTransport $transport */
        $transport = $this->getTransport($transportName);

        return $transport->getMessageCount();
    }
}
