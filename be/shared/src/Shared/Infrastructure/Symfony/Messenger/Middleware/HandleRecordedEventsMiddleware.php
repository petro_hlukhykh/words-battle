<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Middleware;

use Leonix\Shared\Domain\Bus\Event\ContainDomainEvents;
use Leonix\Shared\Domain\Bus\Event\EventBus;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Throwable;

class HandleRecordedEventsMiddleware implements MiddlewareInterface
{
    /**
     * @var EventBus
     */
    private $eventBus;

    /**
     * @var ContainDomainEvents
     */
    private $recorder;

    public function __construct(EventBus $eventBus, ContainDomainEvents $recorder)
    {
        $this->eventBus = $eventBus;
        $this->recorder = $recorder;
    }

    /**
     * @param Envelope $envelope
     * @param StackInterface $stack
     * @return Envelope
     * @throws Throwable
     */
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        try {
            $envelope = $stack->next()->handle($envelope, $stack);

            $recordedEvents = $this->recorder->recordedEvents();
            $this->recorder->eraseEvents();

            foreach ($recordedEvents as $recordedEvent) {
                $this->eventBus->notify($recordedEvent);
            }

            return $envelope;
        } catch (Throwable $exception) {
            if ($exception instanceof HandlerFailedException) {
                // Remove all HandledStamp from the envelope so the retry will execute all handlers again.
                // When a handler fails, the queries of allegedly successful previous handlers just got rolled back.
                throw new HandlerFailedException(
                    $exception->getEnvelope()->withoutAll(HandledStamp::class),
                    $exception->getNestedExceptions()
                );
            }

            throw $exception;
        }
    }
}
