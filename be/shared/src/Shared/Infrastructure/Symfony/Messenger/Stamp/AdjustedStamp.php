<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class AdjustedStamp implements StampInterface
{
    private $time;

    public function __construct()
    {
        $this->time = microtime(true);
    }

    public function adjustedAt(): float
    {
        return $this->time;
    }
}
