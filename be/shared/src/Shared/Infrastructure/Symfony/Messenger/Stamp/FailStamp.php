<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class FailStamp implements StampInterface
{
    private const DEFAULT_MESSAGE = 'Test Fail Message {current}';

    private $failTimes;
    private $failed = 0;
    private $failMessage;

    public function __construct(int $failTimes, ?string $failMessage = null)
    {
        $this->failTimes = $failTimes;
        $this->failMessage = $failMessage ?? self::DEFAULT_MESSAGE;
    }

    public function fail()
    {
        $this->failed++;
    }

    public function isShouldFail(): bool
    {
        return ($this->failTimes - $this->failed) > 0;
    }

    public function failMessage(): string
    {
        return str_replace('{current}', $this->failed, $this->failMessage);
    }
}
