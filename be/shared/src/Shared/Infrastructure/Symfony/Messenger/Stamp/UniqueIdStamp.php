<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Symfony\Messenger\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class UniqueIdStamp implements StampInterface
{
    private $uniqueId;
    private $time;

    public function __construct()
    {
        $this->uniqueId = uniqid();
        $this->time = microtime(true);
    }

    public function uniqueId(): string
    {
        return $this->uniqueId;
    }

    public function time(): float
    {
        return $this->time;
    }
}
