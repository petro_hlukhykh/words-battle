<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class GuzzleHttpClient implements HttpClientInterface, RequestHandlerContract
{
    private Client $client;
    private int $connectTimeout;
    private int $timeout;

    public function __construct(
        Client $client,
        int $connectTimeout,
        int $timeout
    ) {
        $this->client = $client;
        $this->connectTimeout = $connectTimeout;
        $this->timeout = $timeout;
    }

    public function get(string $url): ResponseInterface
    {
        return $this->request('GET', $url);
    }

    /**
     * @inheritDoc
     */
    public function request(
        string $method,
        string $url,
        ?string $body = null,
        array $headers = [],
        bool $stream = false
    ): ResponseInterface {
        try {
            $options = [
                'headers' => $headers,
                'body' => $body,
                'exceptions' => false,
                'connect_timeout' => $this->connectTimeout,
                'timeout' => $this->timeout,
                'stream' => $stream,
            ];

            return $this->client->request($method, $url, $options);
        } catch (RequestException $exception) {
            throw HttpClientException::withResponse(
                $exception->getResponse(),
                $exception->getMessage(),
                $method,
                $url
            );
        } catch (Throwable $exception) {
            throw HttpClientException::causedBy(
                $exception->getMessage(),
                $method,
                $url
            );
        }
    }

    public function post(
        string $url,
        ?string $body = null,
        array $headers = []
    ): ResponseInterface {
        return $this->request('POST', $url, $body, $headers);
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        return $this->request(
            $request->getMethod(),
            (string)$request->getUri(),
            (string)$request->getBody(),
            $request->getHeaders()
        );
    }
}
