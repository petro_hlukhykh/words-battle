<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use RuntimeException;
use Traversable;

final class RequestMiddlewareRegistry
{
    private array $registry;

    public function __construct(iterable $middlewares)
    {
        $middlewares = $middlewares instanceof Traversable ? iterator_to_array($middlewares) : $middlewares;
        array_map(function (RequestMiddlewareContract $middleware): void {
            $this->add($middleware);
        }, $middlewares);
    }

    public function add(RequestMiddlewareContract $middleware): void
    {
        $name = $middleware->name();
        if (isset($this->registry[$name])) {
            throw new RuntimeException("Middleware $name is already registered.");
        }

        $this->registry[$middleware->name()] = $middleware;
    }

    public function get(string $name)
    {
        if (!isset($this->registry[$name])) {
            throw new RuntimeException("Middleware with $name is not registered.");
        }

        return $this->registry[$name];
    }

    public function isRegistered(string $name): bool
    {
        return isset($this->registry[$name]);
    }
}
