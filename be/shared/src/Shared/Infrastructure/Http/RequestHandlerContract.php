<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface RequestHandlerContract
{
    public function handle(RequestInterface $request): ResponseInterface;
}
