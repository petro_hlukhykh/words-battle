<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Throwable;

final class HttpClientException extends RuntimeException
{
    /**
     * @var ResponseInterface|null
     */
    private $response;

    private function __construct(
        string $message,
        string $url,
        string $method,
        ?ResponseInterface $response = null,
        Throwable $previous = null
    ) {
        $this->response = $response;
        $message = sprintf(
            '%s request to URL %s failed with error: %s',
            $method,
            $url,
            $message
        );

        parent::__construct($message, 0, $previous);
    }

    public static function causedBy(
        string $reason,
        string $method,
        string $url
    ): self {
        return new self($reason, $method, $url);
    }

    public static function withResponse(
        ?ResponseInterface $response,
        string $reason,
        string $method,
        string $url
    ): self {
        return new self($reason, $method, $url, $response);
    }

    public function hasResponse(): bool
    {
        return isset($this->response);
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }
}
