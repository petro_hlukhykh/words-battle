<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class MiddlewareHandler implements RequestHandlerContract
{
    private RequestMiddlewareContract $middleware;
    private RequestHandlerContract $handler;

    public function __construct(
        RequestMiddlewareContract $middleware,
        RequestHandlerContract $handler
    ) {
        $this->middleware = $middleware;
        $this->handler = $handler;
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        return $this->middleware->process($request, $this->handler);
    }
}
