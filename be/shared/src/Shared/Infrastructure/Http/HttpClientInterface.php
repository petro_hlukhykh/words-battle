<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Http;

use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @param string $url
     * @return ResponseInterface
     * @throws HttpClientException
     */
    public function get(string $url): ResponseInterface;

    /**
     * @param string $url
     * @param string|null $body
     * @param array $headers
     * @return ResponseInterface
     * @throws HttpClientException
     */
    public function post(
        string $url,
        ?string $body = null,
        array $headers = []
    ): ResponseInterface;

    /**
     * @param string $method
     * @param string $url
     * @param string|null $body
     * @param array $headers
     * @param bool $stream
     * @return ResponseInterface
     * @throws HttpClientException
     */
    public function request(
        string $method,
        string $url,
        ?string $body = null,
        array $headers = [],
        bool $stream = false
    ): ResponseInterface;
}
