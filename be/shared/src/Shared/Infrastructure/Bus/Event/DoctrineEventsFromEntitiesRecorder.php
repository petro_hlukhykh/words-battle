<?php

namespace Leonix\Shared\Infrastructure\Bus\Event;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Proxy\Proxy;
use Leonix\Shared\Domain\Bus\Event\ContainDomainEvents;

class DoctrineEventsFromEntitiesRecorder implements EventSubscriber, ContainDomainEvents
{
    private $collectedEvents = [];

    public function getSubscribedEvents()
    {
        return [
            Events::preFlush,
            Events::postFlush,
        ];
    }

    public function preFlush(PreFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getIdentityMap() as $entities) {
            foreach ($entities as $entity) {
                $this->collectEventsFromEntity($entity);
            }
        }
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            $this->collectEventsFromEntity($entity);
        }
    }

    private function collectEventsFromEntity($entity)
    {
        if (!$entity instanceof ContainDomainEvents) {
            return;
        }

        if ($entity instanceof Proxy && !$entity->__isInitialized__) {
            return;
        }

        foreach ($entity->recordedEvents() as $event) {
            $this->collectedEvents[] = $event;
        }

        $entity->eraseEvents();
    }

    public function recordedEvents(): array
    {
        return $this->collectedEvents;
    }

    public function eraseEvents(): void
    {
        $this->collectedEvents = [];
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getIdentityMap() as $entities) {
            foreach ($entities as $entity) {
                $this->collectEventsFromEntity($entity);
            }
        }
    }
}
