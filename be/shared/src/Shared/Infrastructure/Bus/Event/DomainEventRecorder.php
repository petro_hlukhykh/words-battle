<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Bus\Event;

use Leonix\Shared\Domain\Bus\Event\DomainEvent;

trait DomainEventRecorder
{
    private array $events = [];

    /**
     * {@inheritDoc}
     */
    public function recordedEvents(): array
    {
        return $this->events;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseEvents(): void
    {
        $this->events = [];
    }

    /**
     * Record an event.
     *
     * @param DomainEvent $event
     */
    protected function record(DomainEvent $event): void
    {
        $this->events[] = $event;
    }
}
