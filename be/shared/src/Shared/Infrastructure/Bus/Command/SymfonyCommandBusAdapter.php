<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Bus\Command;

use Exception;
use Leonix\Shared\Domain\Bus\Command\Command;
use Leonix\Shared\Domain\Bus\Command\CommandBus;
use Leonix\Shared\Domain\Bus\Command\CommandNotRegisteredError;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyCommandBusAdapter implements CommandBus
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function dispatch(Command $command, array $context = []): void
    {
        $message = $command;
        if (isset($context['stamps'])) {
            if (!\is_array($context['stamps'])) {
                $context['stamps'] = [$context['stamps']];
            }

            $message = new Envelope($message, $context['stamps']);
        }

        try {
            $this->bus->dispatch($message);
        } catch (NoHandlerForMessageException $unused) {
            throw new CommandNotRegisteredError($command);
        } catch (HandlerFailedException $error) {
            throw $error->getPrevious() ?? $error;
        }
    }
}
