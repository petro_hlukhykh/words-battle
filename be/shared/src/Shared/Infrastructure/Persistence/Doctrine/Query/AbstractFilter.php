<?php

declare(strict_types=1);

namespace Leonix\Shared\Infrastructure\Persistence\Doctrine\Query;

use Doctrine\ORM\QueryBuilder;
use InvalidArgumentException;
use Leonix\Shared\Application\Query\Filter;

abstract class AbstractFilter
{
    protected const FILTERABLE_FIELDS = [];
    protected const PARAM_NORMALIZERS = [];

    public static function filter(string $alias, QueryBuilder $queryBuilder, Filter $filter): QueryBuilder
    {
        if (!isset(static::FILTERABLE_FIELDS[$filter->column()])) {
            return $queryBuilder;
        }

        $column = str_replace('%alias%', $alias, static::FILTERABLE_FIELDS[$filter->column()]);
        $expr = $queryBuilder->expr();
        $param = $filter->params();
        $paramName = ':param_' . $filter->column() . '_' . $queryBuilder->getParameters()->count();
        switch ($filter->operator()) {
            case Filter::OP_EQ:
                $expr = $expr->eq($column, $paramName);
                break;
            case Filter::OP_NEQ:
                $expr = $expr->neq($column, $paramName);
                break;
            case Filter::OP_GT:
                $expr = $expr->gt($column, $paramName);
                break;
            case Filter::OP_GTE:
                $expr = $expr->gte($column, $paramName);
                break;
            case Filter::OP_LT:
                $expr = $expr->lt($column, $paramName);
                break;
            case Filter::OP_LTE:
                $expr = $expr->lte($column, $paramName);
                break;
            case Filter::OP_IN:
                $expr = $expr->in($column, $paramName);
                break;
            case Filter::OP_NOT_IN:
                $expr = $expr->notIn($column, $paramName);
                break;
            case Filter::OP_STARTS_WITH:
                $param .= '%';
                $expr = $expr->like($column, $paramName);
                break;
            case Filter::OP_CONTAINS:
                $param = '%' . $param . '%';
                $expr = $expr->like($column, $paramName);
                break;
            case Filter::OP_ENDS_WITH:
                $param = '%' . $param;
                $expr = $expr->like($column, $paramName);
                break;
            default:
                throw new InvalidArgumentException('Unsupported filter operation');
        }

        $queryBuilder->andWhere($expr)->setParameter(
            $paramName,
            static::PARAM_NORMALIZERS[$filter->column()][$param] ?? $param
        );

        return $queryBuilder;
    }
}
