// src/controllers/slideshow_controller.js
import {Controller} from "@hotwired/stimulus"

export default class extends Controller {
    static targets = ["body"]
    static values = {
        page: {type: Number, default: 1},
        limit: {type: Number, default: 10},
        url: String
    }

    getNext() {
        this.pageValue++
        this.load()
    }

    load() {
        fetch(this.urlValue + '?ajax=1&page=' + this.pageValue + '&limit=' + this.limitValue)
            .then(response => response.text())
            .then(html => $(this.bodyTarget).append(html))
    }
}