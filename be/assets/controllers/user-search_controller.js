// src/controllers/slideshow_controller.js
import {Controller} from "@hotwired/stimulus"

export default class extends Controller {
    static targets = ["inputSearch", "body"]
    static values = {
        limit: {type: Number, default: 10},
        url: String
    }

    search() {
        let q = this.inputSearchTarget.value;
        if (q.length < 3) {
            return;
        }

        fetch(this.urlValue + '?ajax=1&q=&limit=' + this.limitValue + '&q='  + q)
            .then(response => response.text())
            .then(html => $(this.bodyTarget).html(html))
    }
}