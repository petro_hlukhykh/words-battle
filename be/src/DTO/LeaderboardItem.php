<?php

declare(strict_types=1);

namespace App\DTO;

use Leonix\Shared\Application\ConstructableFromArrayTrait;

final class LeaderboardItem
{
    use ConstructableFromArrayTrait;

    public string $login;
    public int $rank;
    public int $elo;

    public function __construct(string $login, int $rank, int $elo)
    {
        $this->login = $login;
        $this->rank = $rank;
        $this->elo = $elo;
    }
}
