<?php

declare(strict_types=1);

namespace App\Query;

use Leonix\Shared\Application\ResultCollectionInterface;

interface UserSearchQueryInterface
{
    public const SEARCH_LIMIT = 10;

    public function search(string $loginOrEmail): ResultCollectionInterface;
}
