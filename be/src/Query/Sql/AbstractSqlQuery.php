<?php

declare(strict_types=1);

namespace App\Query\Sql;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Result;
use InvalidArgumentException;
use Leonix\Shared\Application\ResultCollection;
use Leonix\Shared\Application\ResultCollectionInterface;
use Webmozart\Assert\Assert;

abstract class AbstractSqlQuery
{
    protected Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws Exception
     */
    protected function fetch(array $queries): ResultCollectionInterface
    {
        Assert::minCount($queries, 1);
        $dataQuery = array_pop($queries);
        foreach ($queries as $query) {
            $this->execute($query);
        }

        return new ResultCollection(
            $this->fetchAllAssociative($dataQuery),
            $this->getResultItemClass()
        );
    }

    /**
     * @throws Exception
     */
    protected function fetchAllAssociative($query): array
    {
        if (is_string($query)) {
            return $this->execute($query)->fetchAllAssociative();
        }

        if ($query instanceof QueryBuilder) {
            return $query->executeQuery()->fetchAllAssociative();
        }

        throw new InvalidArgumentException(
            sprintf("Expected query to be string or QueryBuilder, but %s taken", gettype($query))
        );
    }

    /**
     * @throws Exception
     */
    protected function execute($query): Result
    {
        if (is_string($query)) {
            return $this->connection->executeQuery($query);
        }

        if ($query instanceof QueryBuilder) {
            return $query->executeQuery();
        }

        throw new InvalidArgumentException(
            sprintf("Expected query to be string or QueryBuilder, but %s taken", gettype($query))
        );
    }

    protected function getResultItemClass(): ?string
    {
        return null;
    }

    protected function queryBuilder(): QueryBuilder
    {
        return $this->connection->createQueryBuilder();
    }
}
