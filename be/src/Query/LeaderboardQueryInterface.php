<?php

namespace App\Query;

use Countable;
use Leonix\Shared\Application\ResultCollectionInterface;

interface LeaderboardQueryInterface extends Countable
{
    public const PAGINATOR_PER_PAGE = 10;

    public function getPage(int $page = 1, int $limit = 10): ResultCollectionInterface;
}
