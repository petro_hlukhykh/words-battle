<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\VocabularyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=VocabularyRepository::class)
 * @UniqueEntity(fields={"word", "language"})
 * @ORM\Table(
 *     name="vocabulary",
 *     indexes={
 *        @ORM\Index(name="language_idx", columns={"language"})
 *     },
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"word", "language"})}
 * )
 */
class Vocabulary
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Regex(pattern="/^[a-za-я-]+$/", message="Only lowercase letters are allowed")
     */
    private $word;

    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\NotBlank
     * @Assert\Language
     */
    private $language;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank
     */
    private $active = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWord(): ?string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function __toString()
    {
        return $this->word;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
