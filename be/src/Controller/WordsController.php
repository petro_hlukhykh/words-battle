<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WordsController extends AbstractController
{
    /**
     * @Route("/words", name="app_words")
     */
    public function index(): Response
    {
        return $this->render('words/index.html.twig', [
            'controller_name' => 'WordsController',
        ]);
    }
}
