<?php

declare(strict_types=1);

namespace App\Controller;

use App\Query\LeaderboardQueryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LeaderboardController extends AbstractController
{
    /**
     * @Route("/{_locale<%app.supported_locales%>}/", name="homepage")
     * @Route("/{_locale<%app.supported_locales%>}/leaderboard", name="leaderboard")
     */
    public function index(Request $request, LeaderboardQueryInterface $query): Response
    {
        $isAjax = $request->isXmlHttpRequest() || (int)$request->query->get('ajax') === 1;
        return $this->render(
            $isAjax ? 'leaderboard/_page-body.html.twig' : 'leaderboard/index.html.twig',
            [
                'leaderboard' => $query->getPage(
                    $request->query->getInt('page', 1),
                    $request->query->getInt('limit', LeaderboardQueryInterface::PAGINATOR_PER_PAGE),
                )
            ]
        );
    }

    /**
     * @Route("/")
     */
    public function indexNoLocale(): Response
    {
        return $this->redirectToRoute('homepage', ['_locale' => 'en']);
    }
}
